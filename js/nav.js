function toggleMenu() {
  var menu = document.getElementById("nav-menu");
  if (menu.style.display === "block") {
    menu.style.display = "none";
  } else {
    menu.style.display = "block";
  }
}

function onMenuResize(query) {
  var isSmall = query.matches;
  var menu = document.getElementById("nav-menu");

  if (isSmall) {
    // Just became small, make sure that the menu is hidden
    menu.style.display = "none";
  } else {
    // Just became large, make sure that the menu is shown
    menu.style.display = "block";
  }
}

window.addEventListener("DOMContentLoaded", function () {
  var query = window.matchMedia("(max-width: 768px)");
  query.addListener(onMenuResize);
});
