module.exports = {
  plugins: [
    require("postcss-import")({
      plugins: [
        require("stylelint")({
          extends: [
            "stylelint-config-standard",
            "stylelint-config-recess-order",
          ],
          rules: {
            "function-no-unknown": [
              true,
              {
                ignoreFunctions: ["svg-load", "tint"],
              },
            ],
          },
          ignoreFiles: ["css/normalize-*.css", "css/skeleton-*.css"],
        }),
      ],
    }),
    require("postcss-inline-svg"),
    require("postcss-simple-vars"),
    require("cssnano")({
      preset: "default",
    }),
    require("postcss-reporter")({
      clearReportedMessages: true,
    }),
  ],
};
